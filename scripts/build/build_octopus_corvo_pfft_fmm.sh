#!/bin/bash
module add gcc
module add ifort
module add gsl
module add etsf_io
module add lapack
module add netcdf
module add gotoblas2
module add mpibull2
module add blacs
module add scalapack

export CC=mpicc
export FC="mpif90"

# It is important that "/opt/intel/Compiler//11.1/038/bin/intel64/" not to be in the PATH
export PATH=/opt/mpi/mpibull2-1.3.9-14.s/bin:/opt/netcdf/4.0.1/bin:/opt/etsf_io/1.0.2/bin:/opt/gsl//1.13/bin:/opt/intel/composer_xe_2011_sp1.10.319//bin/intel64/:/opt/gcc//4.4/bin:/opt/slurm/bin:/usr/kerberos/bin:/opt/cuda//bin:/usr/local/bin:/bin:/usr/bin

export LIBFM_HOME="$HOME/Software/scafacos"
export PFFT_HOME="$HOME/local/pfft-1.0.5-alpha"
export FFTW3_HOME="$HOME/local/fftw-3.3.2"
export LIBS_PFFT="-L$PFFT_HOME/lib -L$FFTW3_HOME/lib -lpfft -lfftw3_mpi -lfftw3"
export CFLAGS=" -I$PFFT_HOME/include -I$FFTW3_HOME/include -I$LIBFM_HOME/include"
export LDFLAGS=$LDFLAGS" -L$LIBFM_HOME/lib -L$PFFT_HOME/lib -L$FFTW3_HOME/lib -lpfft -lfftw3_mpi -lfftw3 -lm "
export FCFLAGS=$CFLAGS
export CPPFLAGS=" -I$LIBFM_HOME/include "
export LD_LIBRARY_PATH=$LIBFM_HOME/lib:$LD_LIBRARY_PATH

../configure --enable-mpi --with-libxc-prefix=$HOME/Software/libxc_9882 \
--prefix=$HOME/Software/octopus \
--with-blacs=/opt/blacs/1.1/lib/libblacs.a --with-scalapack \
--with-libfm="-L$LIBFM_HOME/lib -lfcs4fortran -lfcs -lfcs_direct -lfcs_fmm -lfcs_near -lfcs_gridsort -lfcs_common" \
--disable-openmp

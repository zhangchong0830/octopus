 #!/bin/bash
 ./configure --enable-fcs-solvers=fmm,direct --enable-fcs-fmm-comm=armci \
   --enable-fcs-fmm-unrolled --enable-fcs-fmm-max-mpol=40 \
   CFLAGS=-O3 CXXFLAGS=-O3 FCFLAGS=-O3 --disable-doc CXX=mpic++ CC=mpicc \
   FC=mpif90 --prefix=$HOME/Software/scafacos \
   LDFLAGS=-L/opt/scalapack/1.8.0/lib -L/opt/blacs/1.1/lib \
   -L/opt/gotoblas2/1.08/lib -L/opt/netcdf/4.0.1/lib -L/opt/lapack/3.2/lib \
   -L/opt/etsf_io/1.0.2/lib --enable-fcs-int=int --enable-fcs-float=double \
   --enable-fcs-integer=integer --enable-fcs-real=real*8

 make
 make install

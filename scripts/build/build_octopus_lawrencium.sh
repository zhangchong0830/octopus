#!/bin/bash
module load icc/10.1.018
module load ifort/10.1.018
module load mkl/2011.1.107
module load openmpi/1.4.3-intel
module load netcdf/4.0-intel
module load fftw/3.1.2-intel
module load autoconf/2.65
module load gsl/1.13-intel
module load gdlib/2.0.35
module load libtool/2.2.6b

./configure --prefix=`pwd` CC=mpicc FC=mpif90 CFLAGS="-O3" FCFLAGS="-O3 -check bounds" --enable-mpi \
--with-blas="-L$MKLROOT/lib/intel64 -Wl,--start-group -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -Wl,--end-group -lpthread" \
--with-fft-lib="-L/global/software/centos-5.x86_64/modules/fftw/3.1.2-intel/lib -lfftw3" --with-netcdf-prefix="$NETCDF" --with-netcdf-include="$NETCDF/include" --enable-newuoa \
--with-libxc-prefix=`pwd` --with-blacs="$MKL_DIR/lib/intel64/libmkl_blacs_openmpi_lp64.a" --with-scalapack="$MKL_DIR/lib/intel64/libmkl_scalapack_lp64.a"
